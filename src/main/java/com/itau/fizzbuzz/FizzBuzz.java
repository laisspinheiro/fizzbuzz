package com.itau.fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

	public static String fizzBuzz(int i) {

//		if (i % 3 == 0) {
//			return "fizz";
//		}
//		return null;
		if ((i % 3 == 0) && (i % 5 == 0)) {
			return "fizzbuzz";
		}

		if (i % 5 == 0) {
			return "buzz";
		}

		if (i % 3 == 0) {
			return "fizz";

		}

		return Integer.toString(i);

	}

	public static List Numeros(int numero) {
		String msg = null;
		List resposta = new ArrayList<String>();
		for (int i = 1; i <= numero; i++) {
			resposta.add( i + " "+ FizzBuzz.fizzBuzz(i));
			

		}
		return resposta;
	}
}
